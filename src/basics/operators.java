package basics;

public class operators {

	/**
	 * @param args
	 */
	public static void main(String[] args) {

		// arithrmatic -- return u values
		
		int i=90;
		int j=56;
		
		System.out.println("i+j : "+ (i+j));
		System.out.println("i-j : "+ (i-j));
		System.out.println("i*j : "+ (i*j));
		System.out.println("i/j : "+ (i/j));
		
		
		// conditional ops -- returns u a boolean
		// >   <   >=    <= ==  !=
		
		System.out.println("i>j : "+ (i>j));
		System.out.println("i<j : "+ (i<j));
		System.out.println("i>=j : "+ (i>=j));
		System.out.println("i<=j : "+ (i<=j));
		System.out.println("i==j : "+ (i==j));
		System.out.println("i!=j : "+ (i!=j));
		
		
		// a, b , c
		// AND &&     OR ||     NOT !
		
		int k=670;
		
		System.out.println("i>j && i>k: "+ (i>j && i>k));
		System.out.println("i>j || i>k: "+ (i>j || i>k));
		System.out.println("! i>j  "+ (! (i>j) ));
		
		
		System.out.println("************* increment ******************");
		int x=10;
	//	++ increment operator
		// x++  post increament
		// ++x pre increment
		
		System.out.println(x++); /// use x , n den do x+1
		System.out.println(x);
		
		
		
		x=10;
		
		
		System.out.println(++x);// do x+1 , use x
		
		System.out.println("************* decrement ******************");

		x=10;
		// -- decrement operator
		// x--   post
		// --x  pre
		
		
		System.out.println(x--);// use x , den do x-1
		
		System.out.println(x);
		
		System.out.println(--x);// do x-1 , den use x
		
		
		
		
		
		
		
		
		
		
		
	}

}
